import React from 'react';
import Loadable from 'react-loadable'


function Loading() {
    //TODO: Change this with an awesome loading spinner
    return <div>Loading...</div>;
}

const Home = Loadable({
  loader: () => import('./views/index.js'),
  loading: Loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: Home },
]

export default routes;
